/*
 * Master-Detail - Exercise 2
 *
 * Def: A master–detail interface displays a master list and the details for the currently selected item. [Wikipedia]
 *
 * The goal of this exercise is to extend Master-Detail - Exercise 1 by adding user friendly interface via Node.js Express Server.
 */

//Naloga 3:

var Sequelize = require('sequelize');
var sequelize = new Sequelize('northwind', 'nodejs', 'nodejs', {dialect: 'mysql',});
var Project = require('sequelize-import')(__dirname + '/../models', sequelize, { exclude: ['index.js']});

Project.Suppliers.hasMany(Project.Products, {foreignKey: 'SupplierID'});
Project.Orders.hasMany(Project.OrderDetails, {foreignKey: 'ProductID'});

// 1. index handler is used when www client connects to localhost:3000 or localhost:3000/index [see app.js, line 35-36]
// 2. [see app.js, line 35-36]
exports.index = function(req, res){

	// 3.  Select an Item
	//
	//    Client uses a hyperlink to select an item. A HTTP GET request is issued (e.g. localhost:3000/index?supplier=1)
	//    The handler can access the supplier variable via server variable req.query.supplier
	var selectedItem = req.query.supplier || 1; // Defaults to 1 if req.query.supplier is undefined
	var selectedProduct = req.query.product || 1;

	// 4. Execute Queries from the Master-Detail - Exercise 1
	Project.Suppliers
		.findAll()
		.success(function(qrm) {

			if(qrm == null)
				throw "Err";
			//console.log(qrm);
			
			Project.Suppliers
				.find({ where: { SupplierID: selectedItem }, include: [Project.Products] })
				.success(function(qrd) {

					if(qrd == null)
						throw "Err";
					//console.log(qrd);
					
					Project.Products
						.find({where: {ProductID: selectedProduct}, include: [Project.OrderDetails]})
						.success(function (qrd1){
							if(qrd1 == null)
								throw "Err";
							
							res.render('index', {title: 'Master-Detail', master: qrm, detail: qrd.Products, selected: selectedItem, orders: qrd1.OrderDetails, selectedP: selectedProduct });
						})
						.error(function(err){
							res.render('index', { title: 'Master-Detail', master: qrm, detail: null, selected: selectedItem, orders: null });
				})
				.error(function(err){
					res.render('index', { title: 'Master-Detail', master: qrm, detail: null, selected: selectedItem });
				});
			});
		})
		.error(function(err){
			render404(req, res);
	});
};

render404 = function(req, res) {
	res.writeHead('Content-Type', 'text/plain');
	res.end("404");
}
